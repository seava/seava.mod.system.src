
  /* ==================== SYS_DS ==================== */
  
  /* Table */
  
  create table if not exists SYS_DS (
    MODEL varchar(255) not null,
    ISASGN int(1) not null,
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table SYS_DS add primary key(ID);
  alter table SYS_DS add constraint UK_SYS_DS_3 unique (MODEL);
  alter table SYS_DS add constraint UK_SYS_DS_1 unique (NAME);
