
  /* ==================== SYS_DT_FMT_MASK ==================== */
  
  /* Table */
  
  create table if not exists SYS_DT_FMT_MASK (
    DT_FMT_ID varchar(64) not null,
    MASK varchar(32) not null,
    VALUE varchar(255) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table SYS_DT_FMT_MASK add primary key(ID);
  alter table SYS_DT_FMT_MASK add constraint UK_SYS_DT_FMT_MASK_1 unique (DT_FMT_ID,MASK);
