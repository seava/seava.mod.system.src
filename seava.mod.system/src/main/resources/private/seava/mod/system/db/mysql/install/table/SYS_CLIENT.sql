
  /* ==================== SYS_CLIENT ==================== */
  
  /* Table */
  
  create table if not exists SYS_CLIENT (
    ADMINROLE varchar(32) not null,
    WORKSPACEPATH varchar(255) not null,
    IMPORTPATH varchar(255) not null,
    EXPORTPATH varchar(255) not null,
    TEMPPATH varchar(255) not null,
    CODE varchar(64) not null,
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table SYS_CLIENT add primary key(ID);
  alter table SYS_CLIENT add constraint UK_SYS_CLIENT_2 unique (CODE);
  alter table SYS_CLIENT add constraint UK_SYS_CLIENT_1 unique (NAME);
