/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DataSourceField_Dc$CtxList", {
  _noImport_:true,
  _noExport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"dataType", dataIndex:"dataType"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.system_DataSourceField_Dc$CtxList"
});
