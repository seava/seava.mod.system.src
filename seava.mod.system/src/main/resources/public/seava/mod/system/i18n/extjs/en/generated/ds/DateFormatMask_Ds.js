Ext.define("seava.mod.system.i18n.extjs.en.generated.ds.DateFormatMask_Ds", {
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  dateFormatId__lbl: "Date Format(ID)",
  dateFormat__lbl: "Date Format",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  mask__lbl: "Mask",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  value__lbl: "Value",
  version__lbl: "Version"
});
