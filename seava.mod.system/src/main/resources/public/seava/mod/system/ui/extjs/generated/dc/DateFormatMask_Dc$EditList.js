/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DateFormatMask_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"dateFormat", dataIndex:"dateFormat", width:100, noEdit:true })
      .addTextColumn({ name:"mask", dataIndex:"mask", width:250, noEdit:true })
      .addTextColumn({ name:"value", dataIndex:"value", width:300, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"dateFormatId", dataIndex:"dateFormatId", hidden:true, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.system_DateFormatMask_Dc$EditList"
});
