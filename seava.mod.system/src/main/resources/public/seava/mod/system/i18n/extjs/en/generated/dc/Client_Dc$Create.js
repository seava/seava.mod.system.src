/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.i18n.extjs.en.generated.dc.Client_Dc$Create",{
  adminPassword__lbl: "Password",
  adminUserCode__lbl: "Code",
  adminUserLogin__lbl: "Login",
  adminUserName__lbl: "Name",
  col2__ttl: "Administrator user",
  col3__ttl: "Working directories"
});
