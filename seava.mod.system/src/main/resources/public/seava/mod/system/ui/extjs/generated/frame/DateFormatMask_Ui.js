/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.DateFormatMask_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("mask", Ext.create(seava.mod.system.ui.extjs.generated.dc.DateFormatMask_Dc,{multiEdit: true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("mask", {name:"maskFilter", xtype:"system_DateFormatMask_Dc$Filter"})
      .addDcView("mask", {name:"maskEditList", xtype:"system_DateFormatMask_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {north:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["maskFilter", "maskEditList"],["north", "center"])
      .addToolbarTo("main", "tlbMaskList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbMaskList", {dc: "mask"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addCancel()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.DateFormatMask_Ui"
});
