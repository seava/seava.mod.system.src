/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DateFormatMask_Dc$Filter", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"dateFormat", bind: "{d.dateFormat}", xtype:"seava.mod.system.ui.extjs.generated.lov.DateFormats_Lov",
        retFieldMapping: [{lovField:"id", dsField: "dateFormatId"} ]})
      .addTextField({ name:"mask", bind: "{d.mask}", maxLength: 32})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:300, layout: {type:"vbox", align:"begin", pack:"start"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1" ])
       .addChildrenTo("col1", ["dateFormat", "mask" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.system_DateFormatMask_Dc$Filter"
});
