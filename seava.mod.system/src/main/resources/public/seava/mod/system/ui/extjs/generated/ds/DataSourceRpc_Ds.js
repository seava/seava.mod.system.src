/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.system.ui.extjs.generated.ds.DataSourceRpc_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "sys_DataSourceRpc_Ds"
  },
  
  fields: [
    {name:"dataSourceId", type:"string"},
    {name:"dataSource", type:"string"},
    {name:"id", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"},
    {name:"notes", type:"string"},
    {name:"active", type:"boolean"},
    {name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"createdBy", type:"string"},
    {name:"modifiedBy", type:"string"},
    {name:"version", type:"int", useNull:true},
    {name:"refid", type:"string"},
    {name:"entityAlias", type:"string"},
    {name:"entityFqn", type:"string"}
  ]
});
