/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.lov.ParamNames_Lov", {
  extend: "seava.lib.e4e.js.lov.AbstractCombo",
  alias: "widget.seava.mod.system.ui.extjs.generated.lov.ParamNames_Lov",
  displayField: "name",
  listConfig: {
    getInnerTpl: function() {
      return '<span>{name}, {code}</span>';
    }
    //width:
  },
  _editFrame_: {
    name: "seava.mod.system.ui.extjs.generated.frame.Parameters_Ui"
  },
  triggers : {
    picker : {
      handler : 'onTriggerClick',
      scope : 'this'
    },
    showFrame : {
      handler : 'onTrigger2Click',
      scope : 'this',
      cls : Ext.baseCSSPrefix + 'form-search-trigger'
    }
  },
  recordModel: "seava.mod.system.ui.extjs.generated.ds.ParamLov_Ds"
});
