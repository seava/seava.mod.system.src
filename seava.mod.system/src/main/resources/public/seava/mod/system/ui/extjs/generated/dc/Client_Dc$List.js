/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Client_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"code", dataIndex:"code", width:120})
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"description", dataIndex:"description", width:200, hidden:true})
      .addTextColumn({ name:"notes", dataIndex:"notes", width:200, hidden:true})
      .addTextColumn({ name:"adminRole", dataIndex:"adminRole"})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addTextColumn({ name:"workspacePath", dataIndex:"workspacePath", width:250})
      .addTextColumn({ name:"importPath", dataIndex:"importPath", width:250, hidden:true})
      .addTextColumn({ name:"exportPath", dataIndex:"exportPath", width:250, hidden:true})
      .addTextColumn({ name:"tempPath", dataIndex:"tempPath", width:250, hidden:true})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.system_Client_Dc$List"
});
