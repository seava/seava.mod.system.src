/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Job_Dc$Filter", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.system.ui.extjs.generated.lov.Jobs_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addTextField({ name:"javaClass", bind: "{d.javaClass}", maxLength: 255})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:300, layout: {type:"vbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col2", width:250, layout: {type:"vbox", align:"begin", pack:"start"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2" ])
       .addChildrenTo("col1", ["name" ])
       .addChildrenTo("col2", ["javaClass" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.system_Job_Dc$Filter"
});
