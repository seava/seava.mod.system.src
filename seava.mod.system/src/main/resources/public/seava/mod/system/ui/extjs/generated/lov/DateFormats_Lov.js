/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.lov.DateFormats_Lov", {
  extend: "seava.lib.e4e.js.lov.AbstractCombo",
  alias: "widget.seava.mod.system.ui.extjs.generated.lov.DateFormats_Lov",
  displayField: "name",
  listConfig: {
    getInnerTpl: function() {
      return '<span>{name}, {description}</span>';
    }
    //width:
  },
  recordModel: "seava.mod.system.ui.extjs.generated.ds.DateFormatLov_Ds"
});
