Ext.define("seava.mod.system.i18n.extjs.en.generated.ds.DataSource_Ds", {
  active__lbl: "Active",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  description__lbl: "Description",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  isAsgn__lbl: "Is Asgn",
  model__lbl: "Model",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  name__lbl: "Name",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  version__lbl: "Version"
});
