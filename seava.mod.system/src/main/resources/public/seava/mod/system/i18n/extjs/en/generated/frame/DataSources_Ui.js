Ext.define("seava.mod.system.i18n.extjs.en.generated.frame.DataSources_Ui", {
  /* view */
  dsFilter__ttl: "Filter",
  fieldsList__ttl: "Fields",
  panelDetails__ttl: "Details",
  rpcsList__ttl: "Services",
  /* menu */
  tlbDsList__ttl: "Data-sources",
  tlbFieldsList__ttl: "Fields",
  tlbRpcsList__ttl: "Services",
  /* button */
  btnInfo__lbl: "Show info",
  btnInfo__tlp: "Show more information.",
  btnSynchronize__lbl: "Synchronize",
  btnSynchronize__tlp: "Scan classpath and synchronize catalog with deployed instances.",
  
  title: "Data-sources"
});
