/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DataSource_Dc$List", {
  _noImport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"model", dataIndex:"model", width:400})
      .addBooleanColumn({ name:"isAsgn", dataIndex:"isAsgn"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.system_DataSource_Dc$List"
});
