/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DateFormatMask_Dc$CtxEditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"mask", dataIndex:"mask", width:250, noEdit:true })
      .addTextColumn({ name:"value", dataIndex:"value", width:200, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"dateFormatId", dataIndex:"dateFormatId", hidden:true, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.system_DateFormatMask_Dc$CtxEditList"
});
