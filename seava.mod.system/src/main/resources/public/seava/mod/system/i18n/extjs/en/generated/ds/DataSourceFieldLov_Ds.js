Ext.define("seava.mod.system.i18n.extjs.en.generated.ds.DataSourceFieldLov_Ds", {
  active__lbl: "Active",
  dataSourceId__lbl: "Data Source(ID)",
  dataSourceName__lbl: "Data Source",
  dataType__lbl: "Data Type",
  description__lbl: "Description",
  id__lbl: "Id",
  name__lbl: "Name",
  refid__lbl: "Refid"
});
