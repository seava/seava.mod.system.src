/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.JobParam_Dc$CtxList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", editor: {xtype: "textfield"}})
      .addTextColumn({ name:"dataType", dataIndex:"dataType", editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.system_JobParam_Dc$CtxList"
});
