/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.DataSource_Dc", {
  extend: "seava.lib.e4e.js.dc.AbstractDcController",
  
  recordModel: "seava.mod.system.ui.extjs.generated.ds.DataSource_Ds"
});
