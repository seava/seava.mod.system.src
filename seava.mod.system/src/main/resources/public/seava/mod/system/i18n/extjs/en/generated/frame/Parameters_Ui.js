Ext.define("seava.mod.system.i18n.extjs.en.generated.frame.Parameters_Ui", {
  /* view */
  sysparamFilter__ttl: "Filter",
  /* menu */
  tlbSysparamList__ttl: "System parameters",
  /* button */
  btnSynchronize__lbl: "Synchronize",
  btnSynchronize__tlp: "Scan classpath and synchronize catalog with parameters declared by modules.",
  
  title: "System parameters"
});
