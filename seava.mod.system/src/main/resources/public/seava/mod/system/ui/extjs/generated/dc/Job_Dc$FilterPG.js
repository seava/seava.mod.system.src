/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Job_Dc$FilterPG", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({ name:"name", dataIndex:"name", editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.Jobs_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
      .addTextColumn({ name:"javaClass", dataIndex:"javaClass", editor: {xtype: "textfield"}})
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterPropGrid",
  alias: "widget.system_Job_Dc$FilterPG"
});
