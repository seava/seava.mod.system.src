/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.DataSources_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("ds", Ext.create(seava.mod.system.ui.extjs.generated.dc.DataSource_Dc,{ autoLoad:true}))
      .addDc("fields", Ext.create(seava.mod.system.ui.extjs.generated.dc.DataSourceField_Dc,{}))
      .addDc("rpcs", Ext.create(seava.mod.system.ui.extjs.generated.dc.DataSourceRpc_Dc,{}))
      .linkDc("fields", "ds",{fetchMode:"auto",
        fields:[{childField:"dataSourceId", parentField:"id"}]})
      .linkDc("rpcs", "ds",{
        fields:[{childField:"dataSourceId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnSynchronize", disabled:false, handler: this.onBtnSynchronize, scope:this})
      .addButton({name:"btnInfo", disabled:true, handler: this.showDsInfo,
          stateManager:{ name:"selected_one", dc:"ds" , and: function(dc) {return ( ! dc.getRecord().get('isAsgn') );}}, scope:this})
      .addDcView("ds", {name:"dsFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"system_DataSource_Dc$FilterV"})
      .addDcView("ds", {name:"dsList", xtype:"system_DataSource_Dc$List"})
      .addDcView("fields", {name:"fieldsList", _hasTitle_:true, xtype:"system_DataSourceField_Dc$CtxList"})
      .addDcView("rpcs", {name:"rpcsList", _hasTitle_:true, xtype:"system_DataSourceRpc_Dc$CtxList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0,east:0}}, defaults:{split:true}})
      .addPanel({name:"panelDetails", _hasTitle_:true, width:450,  collapsible:true, minWidth:300, maxWidth:600, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["dsFilter", "dsList", "panelDetails"],["west", "center", "east"])
      .addChildrenTo("panelDetails", ["fieldsList", "rpcsList"])
      .addToolbarTo("main", "tlbDsList")
      .addToolbarTo("fieldsList", "tlbFieldsList")
      .addToolbarTo("rpcsList", "tlbRpcsList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbDsList", {dc: "ds"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnSynchronize") ,this._elems_.get("btnInfo") ])
        .addReports()
      .end().beginToolbar("tlbFieldsList", {dc: "fields"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbRpcsList", {dc: "rpcs"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnSynchronize
   */
  onBtnSynchronize: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getDc_("ds").doQuery();
    };
    var o={
      name:"synchronizeCatalog",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("ds").doRpcFilter(o);
  },
  
  _afterDefineElements_: function() {
    
    if (!getApplication().getSession().user.systemUser) {
             this._getBuilder_()
        .change("btnSynchronize", {disabled: true});
    }
  },
  
  showDsInfo: function() {
    
    var rd = this._getDc_("ds").getRecord(); 
    var w=window.open( Main.dsAPI(rd.get("name"),"html").info ,"DataSourceInfo","width=600,height=500,scrollbars=yes");
    w.focus();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.DataSources_Ui"
});
