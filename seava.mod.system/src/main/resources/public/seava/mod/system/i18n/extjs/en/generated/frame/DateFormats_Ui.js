Ext.define("seava.mod.system.i18n.extjs.en.generated.frame.DateFormats_Ui", {
  /* view */
  fmtFilter__ttl: "Filter",
  maskEditList__ttl: "Details",
  /* menu */
  tlbFmtList__ttl: "Date formats",
  tlbMaskList__ttl: "Format masks",
  /* button */
  btnSynchronize__lbl: "Synchronize masks",
  btnSynchronize__tlp: "",
  
  title: "Date formats"
});
