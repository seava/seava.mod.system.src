Ext.define("seava.mod.system.i18n.extjs.en.generated.frame.Jobs_Ui", {
  /* view */
  jobFilter__ttl: "Filter",
  /* menu */
  tlbJobList__ttl: "Jobs",
  tlbParamList__ttl: "Parameters",
  /* button */
  btnSynchronize__lbl: "Synchronize",
  btnSynchronize__tlp: "Scan classpath and synchronize catalog with deployed instances.",
  
  title: "Jobs"
});
