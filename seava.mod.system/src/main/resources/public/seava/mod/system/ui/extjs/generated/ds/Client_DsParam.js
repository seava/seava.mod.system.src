/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.system.ui.extjs.generated.ds.Client_DsParam", {
  extend: 'Ext.data.Model',
  
  initParam: function() {
    this.set("adminUserCode", "ADMIN");
    this.set("adminUserName", "Administrator");
    this.set("adminUserLogin", "admin");
    this.set("adminPassword", "admin");
  },
  
  fields: [
    {name:"adminPassword", type:"string"},
    {name:"adminUserCode", type:"string"},
    {name:"adminUserLogin", type:"string"},
    {name:"adminUserName", type:"string"},
    {name:"initFileLocation", type:"string"}
  ]
});
