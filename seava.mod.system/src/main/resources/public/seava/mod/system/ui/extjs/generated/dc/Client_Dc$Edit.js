/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Client_Dc$Edit", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"name", bind: "{d.name}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"code", bind: "{d.code}", allowBlank:false, maxLength: 64})
      .addTextField({ name:"adminRole", bind: "{d.adminRole}", noEdit:true , maxLength: 32})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addTextArea({ name:"description", bind: "{d.description}", height:60})
      .addTextArea({ name:"notes", bind: "{d.notes}", height:60})
      .addTextField({ name:"workspacePath", bind: "{d.workspacePath}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"importPath", bind: "{d.importPath}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"exportPath", bind: "{d.exportPath}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"tempPath", bind: "{d.tempPath}", allowBlank:false, maxLength: 255})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:400, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col2", _hasTitle_: true, width:550, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"fieldset", collapsible:true, border:true, margin:"0 0 0 5"})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2" ])
       .addChildrenTo("col1", ["name", "code", "description", "notes", "adminRole", "active" ])
       .addChildrenTo("col2", ["workspacePath", "importPath", "exportPath", "tempPath" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.system_Client_Dc$Edit"
});
