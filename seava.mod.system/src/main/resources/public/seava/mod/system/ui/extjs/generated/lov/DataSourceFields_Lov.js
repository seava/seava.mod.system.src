/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.lov.DataSourceFields_Lov", {
  extend: "seava.lib.e4e.js.lov.AbstractCombo",
  alias: "widget.seava.mod.system.ui.extjs.generated.lov.DataSourceFields_Lov",
  displayField: "name",
  listConfig: {
    getInnerTpl: function() {
      return '<span>{name}, {description}</span>';
    }
    //width:
  },
  _editFrame_: {
    name: "seava.mod.system.ui.extjs.generated.frame.DataSources_Ui"
  },
  triggers : {
    picker : {
      handler : 'onTriggerClick',
      scope : 'this'
    },
    showFrame : {
      handler : 'onTrigger2Click',
      scope : 'this',
      cls : Ext.baseCSSPrefix + 'form-search-trigger'
    }
  },
  recordModel: "seava.mod.system.ui.extjs.generated.ds.DataSourceFieldLov_Ds"
});
