/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.DateFormats_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("fmt", Ext.create(seava.mod.system.ui.extjs.generated.dc.DateFormat_Dc,{multiEdit: true,  autoLoad:true}))
      .addDc("mask", Ext.create(seava.mod.system.ui.extjs.generated.dc.DateFormatMask_Dc,{multiEdit: true}))
      .linkDc("mask", "fmt",{fetchMode:"auto",
        fields:[{childField:"dateFormatId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnSynchronize", disabled:true, handler: this.onBtnSynchronize,
          stateManager:{ name:"selected_one_clean", dc:"fmt" }, scope:this})
      .addDcView("fmt", {name:"fmtFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"system_DateFormat_Dc$FilterV"})
      .addDcView("fmt", {name:"fmtEditList", xtype:"system_DateFormat_Dc$EditList"})
      .addDcView("mask", {name:"maskEditList", _hasTitle_:true, width:500,  collapsible:true, minWidth:300, maxWidth:600, xtype:"system_DateFormatMask_Dc$CtxEditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0,east:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["fmtFilter", "fmtEditList", "maskEditList"],["west", "center", "east"])
      .addToolbarTo("main", "tlbFmtList")
      .addToolbarTo("maskEditList", "tlbMaskList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbFmtList", {dc: "fmt"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end().beginToolbar("tlbMaskList", {dc: "mask"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnSynchronize
   */
  onBtnSynchronize: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getDc_("mask").doQuery();
    };
    var o={
      name:"synchronizeMasks",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("fmt").doRpcData(o);
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.DateFormats_Ui"
});
