/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.system.ui.extjs.generated.ds.ClientNameLov_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "sys_ClientNameLov_Ds"
  },
  
  fields: [
    {name:"id", type:"string"},
    {name:"code", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"},
    {name:"active", type:"boolean"},
    {name:"refid", type:"string"}
  ]
});
