/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Client_Dc$Create", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"name", bind: "{d.name}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"code", bind: "{d.code}", allowBlank:false, maxLength: 64})
      .addTextArea({ name:"description", bind: "{d.description}", height:60})
      .addTextArea({ name:"notes", bind: "{d.notes}", height:60})
      .addTextField({ name:"adminUserCode", paramIndex:"adminUserCode", bind: "{p.adminUserCode}", allowBlank:false, maxLength: 64})
      .addTextField({ name:"adminUserName", paramIndex:"adminUserName", bind: "{p.adminUserName}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"adminUserLogin", paramIndex:"adminUserLogin", bind: "{p.adminUserLogin}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"adminPassword", paramIndex:"adminPassword", bind: "{p.adminPassword}", allowBlank:false, inputType:"password", maxLength: 255})
      .addTextField({ name:"initFileLocation", paramIndex:"initFileLocation", bind: "{p.initFileLocation}", maxLength: 255})
      .addTextField({ name:"workspacePath", bind: "{d.workspacePath}", allowBlank:false,listeners:{
        change:{scope:this, fn:this.createPathsFromWorkspace}
      }, maxLength: 255})
      .addTextField({ name:"importPath", bind: "{d.importPath}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"exportPath", bind: "{d.exportPath}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"tempPath", bind: "{d.tempPath}", allowBlank:false, maxLength: 255})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:400, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col2", _hasTitle_: true, width:300, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"fieldset", collapsible:true, border:true, margin:"0 0 0 5"})
      .addPanel({ name:"col3", _hasTitle_: true, width:550, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"fieldset", collapsible:true, border:true, margin:"0 0 0 5"})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2", "col3" ])
       .addChildrenTo("col1", ["name", "code", "description", "notes" ])
       .addChildrenTo("col2", ["adminUserCode", "adminUserName", "adminUserLogin", "adminPassword" ])
       .addChildrenTo("col3", ["initFileLocation", "workspacePath", "importPath", "exportPath", "tempPath" ])
  },
  
  createPathsFromWorkspace: function(field,nv,ov) {
    
            var r = this._controller_.record;
            if (nv) {
              r.set("importPath", nv + "/import");
              r.set("exportPath", nv + "/export");
              r.set("tempPath", nv + "/temp");
            }        
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.system_Client_Dc$Create"
});
