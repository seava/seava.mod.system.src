Ext.define("seava.mod.system.i18n.extjs.en.generated.ds.Client_DsParam", {
  adminPassword__lbl: "Admin Password",
  adminUserCode__lbl: "Admin User Code",
  adminUserLogin__lbl: "Admin User Login",
  adminUserName__lbl: "Admin User Name",
  initFileLocation__lbl: "Init File Location"
});
