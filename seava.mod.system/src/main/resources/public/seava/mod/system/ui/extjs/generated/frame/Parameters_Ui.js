/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.Parameters_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("sysparam", Ext.create(seava.mod.system.ui.extjs.generated.dc.Param_Dc,{ autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnSynchronize", disabled:false, handler: this.onBtnSynchronize, scope:this})
      .addDcView("sysparam", {name:"sysparamFilter", _hasTitle_:true, width:250,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"system_Param_Dc$FilterV"})
      .addDcView("sysparam", {name:"sysparamList", xtype:"system_Param_Dc$List"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["sysparamFilter", "sysparamList"],["west", "center"])
      .addToolbarTo("main", "tlbSysparamList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbSysparamList", {dc: "sysparam"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnSynchronize") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnSynchronize
   */
  onBtnSynchronize: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getDc_("sysparam").doQuery();
    };
    var o={
      name:"synchronizeCatalog",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("sysparam").doRpcFilter(o);
  },
  
  _afterDefineElements_: function() {
    
    if (!getApplication().getSession().user.systemUser) {
             this._getBuilder_()
        .change("btnSynchronize", {disabled: true});
    }
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Parameters_Ui"
});
