/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.Jobs_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("job", Ext.create(seava.mod.system.ui.extjs.generated.dc.Job_Dc,{}))
      .addDc("params", Ext.create(seava.mod.system.ui.extjs.generated.dc.JobParam_Dc,{}))
      .linkDc("params", "job",{fetchMode:"auto",
        fields:[{childField:"jobId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnSynchronize", disabled:false, handler: this.onBtnSynchronize, scope:this})
      .addDcView("job", {name:"jobFilter", _hasTitle_:true, width:250,  collapsible:true, xtype:"system_Job_Dc$FilterPG"})
      .addDcView("job", {name:"jobList", xtype:"system_Job_Dc$List"})
      .addDcView("params", {name:"paramsCtxList", width:400, xtype:"system_JobParam_Dc$CtxList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0,east:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["jobFilter", "jobList", "paramsCtxList"],["west", "center", "east"])
      .addToolbarTo("main", "tlbJobList")
      .addToolbarTo("paramsCtxList", "tlbParamList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbJobList", {dc: "job"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnSynchronize") ])
        .addReports()
      .end().beginToolbar("tlbParamList", {dc: "params"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnSynchronize
   */
  onBtnSynchronize: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getDc_("job").doQuery();
    };
    var o={
      name:"synchronizeCatalog",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("job").doRpcFilter(o);
  },
  
  _when_called_to_view_: function(params) {
    
    var job = this._getDc_("job");
    if (job.isDirty()) {
    	this._alert_dirty_();
    	return;
    }
    job.doClearQuery();
    	 
    job.setFilterValue("jobName", params.jobName );
    job.doQuery();
  },
  
  _afterDefineElements_: function() {
    
    if (!getApplication().getSession().user.systemUser) {
             this._getBuilder_()
        .change("btnSynchronize", {disabled: true});
    }
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Jobs_Ui"
});
