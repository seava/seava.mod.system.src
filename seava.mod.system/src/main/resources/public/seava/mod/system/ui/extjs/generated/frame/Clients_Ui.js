/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.frame.Clients_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("client", Ext.create(seava.mod.system.ui.extjs.generated.dc.Client_Dc,{ autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("client", {name:"clientFilter", _hasTitle_:true, width:250,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"system_Client_Dc$FilterV"})
      .addDcView("client", {name:"clientList", xtype:"system_Client_Dc$List"})
      .addDcView("client", {name:"clientEdit", xtype:"system_Client_Dc$Edit", noInsert:true})
      .addDcView("client", {name:"clientCreate", xtype:"system_Client_Dc$Create", noUpdate:true})
      .addPanel({name:"main", layout:{ type: "card" }, activeItem:0})
      .addPanel({name:"canvas1", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"canvas2", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {center:0}}, defaults:{split:true}})
      .addPanel({name:"canvas3", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["canvas1", "canvas2", "canvas3"])
      .addChildrenTo("canvas1", ["clientFilter", "clientList"],["west", "center"])
      .addChildrenTo("canvas2", ["clientEdit"],["center"])
      .addChildrenTo("canvas3", ["clientCreate"],["center"])
      .addToolbarTo("canvas1", "tlbClientList")
      .addToolbarTo("canvas2", "tlbClientEdit")
      .addToolbarTo("canvas3", "tlbClientCreate")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbClientList", {dc: "client"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addEdit().addNew({autoEdit:"false",showView:"canvas3"})
        .addReports()
      .end().beginToolbar("tlbClientEdit", {dc: "client"})
        .addTitle().addSeparator().addSeparator()
        .addBack().addSave().addNew({autoEdit:"false",showView:"canvas3"}).addCancel().addPrevRec().addNextRec()
        .addReports()
      .end().beginToolbar("tlbClientCreate", {dc: "client"})
        .addTitle().addSeparator().addSeparator()
        .addBack().addSave().addNew({autoEdit:"false",showView:"canvas3"}).addCancel().addPrevRec().addNextRec()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Clients_Ui"
});
