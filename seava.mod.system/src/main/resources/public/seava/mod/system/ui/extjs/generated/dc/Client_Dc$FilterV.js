/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.system.ui.extjs.generated.dc.Client_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.system.ui.extjs.generated.lov.Clients_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.system.ui.extjs.generated.lov.ClientNames_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["code", "name", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.system_Client_Dc$FilterV"
});
