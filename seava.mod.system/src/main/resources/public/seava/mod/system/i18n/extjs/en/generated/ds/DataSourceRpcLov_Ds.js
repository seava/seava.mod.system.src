Ext.define("seava.mod.system.i18n.extjs.en.generated.ds.DataSourceRpcLov_Ds", {
  active__lbl: "Active",
  dataSourceId__lbl: "Data Source(ID)",
  dataSourceName__lbl: "Data Source",
  description__lbl: "Description",
  id__lbl: "Id",
  name__lbl: "Name",
  refid__lbl: "Refid"
});
