/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_impl.extensions.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import seava.lib.j4e.api.base.descriptor.ISysParamDefinition;
import seava.lib.j4e.api.base.descriptor.ISysParamDefinitions;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.mod.system.business_api.generated.spi.IParamService;
import seava.mod.system.domain.generated.model.Param;

/**
 * Service class implementation for {@link Param} domain entity. <br>
 * Contains repository functionality with finder methods as well as specific
 * business functionality
 */
public class Param_Service extends
		seava.mod.system.business_impl.generated.service.Param_Service
		implements IParamService {

	/**
	 * 
	 */
	@Transactional
	public void doSynchronizeCatalog() throws ManagedException {
		List<ISysParamDefinitions> defs = this.getSettings()
				.getParamDefinitions();
		List<Param> entities = new ArrayList<Param>();
		for (ISysParamDefinitions def : defs) {
			for (ISysParamDefinition d : def.getSysParamDefinitions()) {
				Param e = new Param();
				e.setActive(true);
				e.setCode(d.getName());
				e.setName(d.getTitle());
				e.setDefaultValue(d.getDefaultValue());
				e.setListOfValues(d.getListOfValues());
				e.setDescription(d.getDescription());
				entities.add(e);
			}
		}
		this.update("delete from " + Param.ALIAS, null);
		this.insert(entities);

	}
}
