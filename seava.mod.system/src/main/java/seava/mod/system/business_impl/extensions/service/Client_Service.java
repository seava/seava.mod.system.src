/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_impl.extensions.service;

import java.io.File;

import org.springframework.transaction.annotation.Transactional;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IInitializeClientService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.mod.system.business_api.generated.spi.IClientService;
import seava.mod.system.domain.generated.model.Client;

/**
 * Service class implementation for {@link Client} domain entity. <br>
 * Contains repository functionality with finder methods as well as specific
 * business functionality
 */
public class Client_Service extends
		seava.mod.system.business_impl.generated.service.Client_Service
		implements IClientService {

	/**
	 * 
	 */
	@Override
	@Transactional
	public void doInsertWithUserAccount(Client client, String userCode,
			String userName, String loginName, String password)
			throws ManagedException {
		this.createClient(client);
		this.getApplicationContext()
				.getBean(IInitializeClientService.class)
				.execute(client.getId(), client.getCode(), userCode, userName,
						loginName, password, null);
	}

	/**
	 * 
	 */
	@Override
	@Transactional
	public void doInsertWithUserAccountAndSetup(Client client, String userCode,
			String userName, String loginName, String password,
			IImportDataPackage dataPackage) throws ManagedException {
		this.createClient(client);
		this.getApplicationContext()
				.getBean(IInitializeClientService.class)
				.execute(client.getId(), client.getCode(), userCode, userName,
						loginName, password, dataPackage);

	}

	/**
	 * 
	 * @param client
	 * @throws ManagedException
	 */
	protected void createClient(Client client) throws ManagedException {
		this.preSave(client);
		client.setAdminRole(Constants.ROLE_ADMIN_CODE);
		client.setActive(true);
		this.getEntityManager().persist(client);
	}

	@Override
	protected void preInsert(Client e) throws ManagedException {
		this.preSave(e);
	}

	@Override
	protected void preUpdate(Client e) throws ManagedException {
		this.preSave(e);
	}

	protected void preSave(Client e) throws ManagedException {
		this.validatePath(e.getWorkspacePath(), "Workspace");
		this.validatePath(e.getImportPath(), "Import");
		this.validatePath(e.getExportPath(), "Export");
		this.validatePath(e.getTempPath(), "Temporary");
	}

	protected void validatePath(String path, String name)
			throws ManagedException {
		if (path == null) {
			throw new ManagedException(ErrorCodeCommons.INVALID_FILE_LOCATION,
					name + " path cannot be empty.");
		}
		File f = new File(path);
		if (!f.isAbsolute()) {
			throw new ManagedException(ErrorCodeCommons.INVALID_FILE_LOCATION,
					name + " path `" + path + "` must be an absolute path.");
		}

		if (!f.exists()) {
			if (!f.mkdirs()) {
				throw new ManagedException(ErrorCodeCommons.FILE_NOT_CREATABLE,
						name + " path `" + path
								+ "` structure cannot be created.");
			}
		} else {
			if (!f.canRead()) {
				throw new ManagedException(ErrorCodeCommons.FILE_NOT_READABLE,
						name + " path `" + path + "` is not read enabled.");
			}
			if (!f.canWrite()) {
				throw new ManagedException(ErrorCodeCommons.FILE_NOT_WRITABLE,
						name + " path `" + path + "` is not write enabled.");
			}
		}
	}
}
