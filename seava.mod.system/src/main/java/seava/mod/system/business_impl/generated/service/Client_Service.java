/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_impl.generated.service;

import javax.persistence.EntityManager;
import seava.lib.j4e.business.service.entity.AbstractEntityService;
import seava.mod.system.domain.generated.model.Client;

/**
 * Service class implementation for {@link Client} domain
 * entity. <br>
 * Contains repository functionality with finder methods as well as specific business functionality
 */
public class Client_Service extends AbstractEntityService<Client> {
  
  public Client_Service() {
    super();
  }
  
  public Client_Service(EntityManager em) {
    super();
    this.setEntityManager(em);
  }
  
  @Override
  public Class<Client> getEntityClass() {
    return Client.class;
  }
}
