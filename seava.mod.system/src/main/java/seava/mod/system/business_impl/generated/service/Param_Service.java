/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_impl.generated.service;

import javax.persistence.EntityManager;
import seava.lib.j4e.business.service.entity.AbstractEntityService;
import seava.mod.system.domain.generated.model.Param;

/**
 * Service class implementation for {@link Param} domain
 * entity. <br>
 * Contains repository functionality with finder methods as well as specific business functionality
 */
public class Param_Service extends AbstractEntityService<Param> {
  
  public Param_Service() {
    super();
  }
  
  public Param_Service(EntityManager em) {
    super();
    this.setEntityManager(em);
  }
  
  @Override
  public Class<Param> getEntityClass() {
    return Param.class;
  }
}
