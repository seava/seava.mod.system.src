/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;
import seava.mod.system.domain.generated.model.DataSource;

@Entity(name=DataSourceRpc.ALIAS)
@Table(name=DataSourceRpc.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DataSourceRpc.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+DataSourceRpc.ALIAS+" e WHERE e.dataSource = :dataSource and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=DataSourceRpc.NQ_FIND_BY_NAME_PRIMITIVE, query="SELECT e FROM "+DataSourceRpc.ALIAS+" e WHERE e.dataSource.id = :dataSourceId and e.name = :name", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class DataSourceRpc extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_DataSourceRpc";
  
  public static final String TABLE_NAME = "SYS_DS_RPC";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "DataSourceRpc.findByName";
  /**
   * Named query find by unique key: Name using the ID field for references.
   */
  public static final String NQ_FIND_BY_NAME_PRIMITIVE = "DataSourceRpc.findByName_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DataSource.class)
  @JoinColumn(name="DS_ID", referencedColumnName="ID")
  private DataSource dataSource;
  
  public DataSource getDataSource() {
    return this.dataSource;
  }
  
  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
