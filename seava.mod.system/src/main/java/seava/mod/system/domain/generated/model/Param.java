/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCodeNT;

@Entity(name=Param.ALIAS)
@Table(name=Param.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Param.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+Param.ALIAS+" e WHERE e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=Param.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Param.ALIAS+" e WHERE e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Param extends AbstractTypeWithCodeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_Param";
  
  public static final String TABLE_NAME = "SYS_PARAM";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "Param.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Param.findByName";
      
  
  @Column(name="DEF_VAL")
  private String defaultValue;
  
  @Column(name="LIST_VAL")
  private String listOfValues;
  
  public String getDefaultValue() {
    return this.defaultValue;
  }
  
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }
  
  public String getListOfValues() {
    return this.listOfValues;
  }
  
  public void setListOfValues(String listOfValues) {
    this.listOfValues = listOfValues;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
