/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractAuditableNT;
import seava.mod.system.domain.generated.model.DateFormat;

@Entity(name=DateFormatMask.ALIAS)
@Table(name=DateFormatMask.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DateFormatMask.NQ_FIND_BY_MASK,
    query="SELECT e FROM "+DateFormatMask.ALIAS+" e WHERE e.dateFormat = :dateFormat and e.mask = :mask",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=DateFormatMask.NQ_FIND_BY_MASK_PRIMITIVE, query="SELECT e FROM "+DateFormatMask.ALIAS+" e WHERE e.dateFormat.id = :dateFormatId and e.mask = :mask", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class DateFormatMask extends AbstractAuditableNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_DateFormatMask";
  
  public static final String TABLE_NAME = "SYS_DT_FMT_MASK";
  /**
   * Named query find by unique key: Mask.
   */
  public static final String NQ_FIND_BY_MASK = "DateFormatMask.findByMask";
  /**
   * Named query find by unique key: Mask using the ID field for references.
   */
  public static final String NQ_FIND_BY_MASK_PRIMITIVE = "DateFormatMask.findByMask_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DateFormat.class)
  @JoinColumn(name="DT_FMT_ID", referencedColumnName="ID")
  private DateFormat dateFormat;
  
  @Column(name="MASK", nullable=false)
  private String mask;
  
  @Column(name="VALUE", nullable=false)
  private String value;
  
  public DateFormat getDateFormat() {
    return this.dateFormat;
  }
  
  public void setDateFormat(DateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }
  
  public String getMask() {
    return this.mask;
  }
  
  public void setMask(String mask) {
    this.mask = mask;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
