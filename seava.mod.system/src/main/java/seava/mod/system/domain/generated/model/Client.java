/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCodeNT;

@Entity(name=Client.ALIAS)
@Table(name=Client.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Client.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+Client.ALIAS+" e WHERE e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=Client.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Client.ALIAS+" e WHERE e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Client extends AbstractTypeWithCodeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_Client";
  
  public static final String TABLE_NAME = "SYS_CLIENT";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "Client.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Client.findByName";
      
  
  @Column(name="ADMINROLE", nullable=false)
  private String adminRole;
  
  @Column(name="WORKSPACEPATH", nullable=false)
  private String workspacePath;
  
  @Column(name="IMPORTPATH", nullable=false)
  private String importPath;
  
  @Column(name="EXPORTPATH", nullable=false)
  private String exportPath;
  
  @Column(name="TEMPPATH", nullable=false)
  private String tempPath;
  
  public String getAdminRole() {
    return this.adminRole;
  }
  
  public void setAdminRole(String adminRole) {
    this.adminRole = adminRole;
  }
  
  public String getWorkspacePath() {
    return this.workspacePath;
  }
  
  public void setWorkspacePath(String workspacePath) {
    this.workspacePath = workspacePath;
  }
  
  public String getImportPath() {
    return this.importPath;
  }
  
  public void setImportPath(String importPath) {
    this.importPath = importPath;
  }
  
  public String getExportPath() {
    return this.exportPath;
  }
  
  public void setExportPath(String exportPath) {
    this.exportPath = exportPath;
  }
  
  public String getTempPath() {
    return this.tempPath;
  }
  
  public void setTempPath(String tempPath) {
    this.tempPath = tempPath;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
