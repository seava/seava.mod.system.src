/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;
import seava.mod.system.domain.generated.model.JobParam;

@Entity(name=Job.ALIAS)
@Table(name=Job.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Job.NQ_FIND_BY_JCLASS,
    query="SELECT e FROM "+Job.ALIAS+" e WHERE e.javaClass = :javaClass",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=Job.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Job.ALIAS+" e WHERE e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Job extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_Job";
  
  public static final String TABLE_NAME = "SYS_JOB";
  /**
   * Named query find by unique key: Jclass.
   */
  public static final String NQ_FIND_BY_JCLASS = "Job.findByJclass";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Job.findByName";
      
  
  @Column(name="JAVACLASS", nullable=false)
  private String javaClass;
  
  @OneToMany(fetch=FetchType.LAZY, targetEntity=JobParam.class, mappedBy="job"
  ,cascade=CascadeType.ALL)
  @CascadeOnDelete
  private Collection<JobParam> params;
  
  public String getJavaClass() {
    return this.javaClass;
  }
  
  public void setJavaClass(String javaClass) {
    this.javaClass = javaClass;
  }
  
  public Collection<JobParam> getParams() {
    return this.params;
  }
  
  public void setParams(Collection<JobParam> params) {
    this.params = params;
  }
  
  public void addToParams(JobParam e) {
    if (this.params == null) {
      this.params = new ArrayList<JobParam>();
    }
    e.setJob(this);
    this.params.add(e);
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
