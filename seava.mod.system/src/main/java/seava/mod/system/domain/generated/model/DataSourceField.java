/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;
import seava.mod.system.domain.generated.model.DataSource;

@Entity(name=DataSourceField.ALIAS)
@Table(name=DataSourceField.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DataSourceField.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+DataSourceField.ALIAS+" e WHERE e.dataSource = :dataSource and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=DataSourceField.NQ_FIND_BY_NAME_PRIMITIVE, query="SELECT e FROM "+DataSourceField.ALIAS+" e WHERE e.dataSource.id = :dataSourceId and e.name = :name", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class DataSourceField extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_DataSourceField";
  
  public static final String TABLE_NAME = "SYS_DS_FLD";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "DataSourceField.findByName";
  /**
   * Named query find by unique key: Name using the ID field for references.
   */
  public static final String NQ_FIND_BY_NAME_PRIMITIVE = "DataSourceField.findByName_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DataSource.class)
  @JoinColumn(name="DS_ID", referencedColumnName="ID")
  private DataSource dataSource;
  
  @Column(name="DATATYPE", nullable=false)
  private String dataType;
  
  public DataSource getDataSource() {
    return this.dataSource;
  }
  
  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
  
  public String getDataType() {
    return this.dataType;
  }
  
  public void setDataType(String dataType) {
    this.dataType = dataType;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
