/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;
import seava.mod.system.domain.generated.model.Job;

@Entity(name=JobParam.ALIAS)
@Table(name=JobParam.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=JobParam.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+JobParam.ALIAS+" e WHERE e.job = :job and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=JobParam.NQ_FIND_BY_NAME_PRIMITIVE, query="SELECT e FROM "+JobParam.ALIAS+" e WHERE e.job.id = :jobId and e.name = :name", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class JobParam extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_JobParam";
  
  public static final String TABLE_NAME = "SYS_JOB_PARAM";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "JobParam.findByName";
  /**
   * Named query find by unique key: Name using the ID field for references.
   */
  public static final String NQ_FIND_BY_NAME_PRIMITIVE = "JobParam.findByName_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=Job.class)
  @JoinColumn(name="JOB_ID", referencedColumnName="ID")
  private Job job;
  
  @Column(name="DATATYPE", nullable=false)
  private String dataType;
  
  public Job getJob() {
    return this.job;
  }
  
  public void setJob(Job job) {
    this.job = job;
  }
  
  public String getDataType() {
    return this.dataType;
  }
  
  public void setDataType(String dataType) {
    this.dataType = dataType;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
