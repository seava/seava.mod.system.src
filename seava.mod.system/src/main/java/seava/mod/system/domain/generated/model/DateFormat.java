/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.domain.generated.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeNT;
import seava.mod.system.domain.generated.model.DateFormatMask;

@Entity(name=DateFormat.ALIAS)
@Table(name=DateFormat.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DateFormat.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+DateFormat.ALIAS+" e WHERE e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class DateFormat extends AbstractTypeNT {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "sys_DateFormat";
  
  public static final String TABLE_NAME = "SYS_DT_FMT";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "DateFormat.findByName";
      
  
  @OneToMany(fetch=FetchType.LAZY, targetEntity=DateFormatMask.class, mappedBy="dateFormat"
  ,cascade=CascadeType.ALL)
  @CascadeOnDelete
  private Collection<DateFormatMask> masks;
  
  public Collection<DateFormatMask> getMasks() {
    return this.masks;
  }
  
  public void setMasks(Collection<DateFormatMask> masks) {
    this.masks = masks;
  }
  
  public void addToMasks(DateFormatMask e) {
    if (this.masks == null) {
      this.masks = new ArrayList<DateFormatMask>();
    }
    e.setDateFormat(this);
    this.masks.add(e);
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
