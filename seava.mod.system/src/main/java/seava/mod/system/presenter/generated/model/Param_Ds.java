/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeNT_Ds;
import seava.mod.system.domain.generated.model.Param;

@Ds(entity=Param.class, sort={@SortField(field=Param_Ds.f_code)})
public class Param_Ds extends AbstractTypeWithCodeNT_Ds<Param> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_Param_Ds";
	
	public static final String f_defaultValue = "defaultValue";
	public static final String f_listOfValues = "listOfValues";
	
	@DsField
	private String defaultValue;
	
	@DsField
	private String listOfValues;
	
	public Param_Ds() {
		super();
	}
	
	public Param_Ds(Param e) {
		super(e);
	}
	
	public String getDefaultValue() {
	  return this.defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
	  this.defaultValue = defaultValue;
	}
	
	public String getListOfValues() {
	  return this.listOfValues;
	}
	
	public void setListOfValues(String listOfValues) {
	  this.listOfValues = listOfValues;
	}
}
