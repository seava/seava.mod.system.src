/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.DataSourceField;

@Ds(entity=DataSourceField.class, sort={@SortField(field=DataSourceFieldLov_Ds.f_name)})
public class DataSourceFieldLov_Ds extends AbstractTypeNTLov_Ds<DataSourceField> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceFieldLov_Ds";
	
	public static final String f_dataType = "dataType";
	public static final String f_dataSourceId = "dataSourceId";
	public static final String f_dataSourceName = "dataSourceName";
	
	@DsField
	private String dataType;
	
	@DsField(join="left", path="dataSource.id")
	private String dataSourceId;
	
	@DsField(join="left", path="dataSource.name")
	private String dataSourceName;
	
	public DataSourceFieldLov_Ds() {
		super();
	}
	
	public DataSourceFieldLov_Ds(DataSourceField e) {
		super(e);
	}
	
	public String getDataType() {
	  return this.dataType;
	}
	
	public void setDataType(String dataType) {
	  this.dataType = dataType;
	}
	
	public String getDataSourceId() {
	  return this.dataSourceId;
	}
	
	public void setDataSourceId(String dataSourceId) {
	  this.dataSourceId = dataSourceId;
	}
	
	public String getDataSourceName() {
	  return this.dataSourceName;
	}
	
	public void setDataSourceName(String dataSourceName) {
	  this.dataSourceName = dataSourceName;
	}
}
