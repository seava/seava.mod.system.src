/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.service;

import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.presenter.service.ds.AbstractEntityDsService;
import seava.mod.system.domain.generated.model.Client;
import seava.mod.system.presenter.generated.model.Client_Ds;
import seava.mod.system.presenter.generated.model.Client_DsParam;

public class Client_DsService extends AbstractEntityDsService<Client_Ds, Client_Ds, Client_DsParam, Client> implements IDsService<Client_Ds,Client_Ds, Client_DsParam> {
    
  
}
