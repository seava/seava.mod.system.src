package seava.mod.system.presenter.extensions;

import java.util.Map;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.extensions.IExtensionContentProvider;
import seava.lib.j4e.api.presenter.extensions.IExtensions;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * Provide system menus
 * 
 * @author amathe
 * 
 */
public class ExtensionProviderSystemMenu extends AbstractPresenterBaseService
		implements IExtensionContentProvider {

	/**
	 * Available system menus
	 */
	private Map<String, String> menus;

	public String getContent(String targetName) throws ManagedException {
		String result = "";
		if (Session.user.get().isSystemUser()
				&& IExtensions.UI_EXTJS_MAIN.equals(targetName)) {
			StringBuffer sb = new StringBuffer();
			this.buildContent(sb);
			result = sb.toString();
		}
		return result;
	}

	/**
	 * Build javascript content
	 * 
	 * @param sb
	 * @throws ManagedException
	 */
	protected void buildContent(StringBuffer sb) throws ManagedException {

		sb.append("Main.systemMenus = [");
		for (Map.Entry<String, String> e : menus.entrySet()) {
			sb.append("{\"bundle\":\"seava.mod.ad\", \"frame\":\"" + e.getKey()
					+ "\", \"labelKey\":\"" + e.getValue() + "\" },");
		}
		sb.append("];");
	}

	public Map<String, String> getMenus() {
		return menus;
	}

	public void setMenus(Map<String, String> menus) {
		this.menus = menus;
	}

}
