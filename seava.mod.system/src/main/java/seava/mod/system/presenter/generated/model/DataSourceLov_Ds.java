/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.DataSource;

@Ds(entity=DataSource.class, sort={@SortField(field=DataSourceLov_Ds.f_name)})
public class DataSourceLov_Ds extends AbstractTypeNTLov_Ds<DataSource> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceLov_Ds";
	
	
	public DataSourceLov_Ds() {
		super();
	}
	
	public DataSourceLov_Ds(DataSource e) {
		super(e);
	}
}
