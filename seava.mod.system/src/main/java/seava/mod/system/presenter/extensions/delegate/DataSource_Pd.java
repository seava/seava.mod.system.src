/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.extensions.delegate;

import java.util.ArrayList;
import java.util.List;

import seava.lib.j4e.api.Constants;

import seava.lib.j4e.api.base.descriptor.IFieldDefinition;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.descriptor.IDsDefinition;
import seava.lib.j4e.api.presenter.descriptor.IDsDefinitions;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.viewmodel.DsDefinition;
import seava.lib.j4e.presenter.service.AbstractPresenterDelegate;
import seava.mod.system.domain.generated.model.DataSource;
import seava.mod.system.domain.generated.model.DataSourceField;
import seava.mod.system.domain.generated.model.DataSourceRpc;
import seava.mod.system.presenter.generated.model.DataSource_Ds;

public class DataSource_Pd extends AbstractPresenterDelegate {

	/**
	 * Scan spring xml files to discover data-source definition and store them
	 * in database for easier access.
	 * 
	 * @param filter
	 * @throws Exception
	 */

	public void synchronizeCatalog(DataSource_Ds filter)
			throws ManagedException {

		if (!Session.user.get().isSystemUser()) {
			throw new ManagedException(ErrorCodeCommons.OPERATION_NOT_ALLOWED,
					"This operation is allowed only for system users.");
		}

		IEntityService<DataSource> srv = (IEntityService<DataSource>) this
				.findEntityService(DataSource.class);

		List<DataSource> result = new ArrayList<DataSource>();

		if (this.getSettings().get(Constants.PROP_DEPLOYMENT)
				.equals(Constants.PROP_DEPLOYMENT_JEE)) {
			IDsDefinitions defs = this.getApplicationContext().getBean(
					IDsDefinitions.class);
			this._process(defs, result);
		} else {
			@SuppressWarnings("unchecked")
			List<IDsDefinitions> list = (List<IDsDefinitions>) this
					.getApplicationContext().getBean(
							Constants.SPRING_OSGI_DS_DEFINITIONS);
			for (IDsDefinitions defs : list) {
				this._process(defs, result);
			}
		}
		srv.update("delete from " + DataSourceField.ALIAS, null);
		srv.update("delete from " + DataSourceRpc.ALIAS, null);
		srv.update("delete from " + DataSource.ALIAS, null);
		srv.insert(result);
	}

	/**
	 * 
	 * @param defs
	 * @param result
	 * @throws Exception
	 */
	private void _process(IDsDefinitions defs, List<DataSource> result)
			throws ManagedException {
		for (IDsDefinition def : defs.getDsDefinitions()) {
			DataSource e = new DataSource();
			e.setName(def.getName());
			e.setModel(def.getModelClass().getCanonicalName());
			e.setActive(true);
			e.setIsAsgn(def.isAsgn());

			for (IFieldDefinition fld : ((DsDefinition) def).getModelFields()) {
				DataSourceField f = new DataSourceField();
				f.setName(fld.getName());
				f.setActive(true);
				f.setDataType(fld.getClassName());
				e.addToFields(f);
			}

			List<String> serviceMethods = def.getServiceMethods();
			if (serviceMethods != null) {
				for (String sm : serviceMethods) {
					DataSourceRpc sme = new DataSourceRpc();
					sme.setActive(true);
					sme.setDataSource(e);
					sme.setName(sm);
					e.addToServiceMethods(sme);
				}
			}
			result.add(e);
		}
	}
}
