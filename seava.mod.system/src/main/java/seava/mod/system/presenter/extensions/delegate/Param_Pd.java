/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.extensions.delegate;

import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.presenter.service.AbstractPresenterDelegate;
import seava.mod.system.business_api.generated.spi.IParamService;
import seava.mod.system.domain.generated.model.Param;
import seava.mod.system.presenter.generated.model.Param_Ds;

public class Param_Pd extends AbstractPresenterDelegate {

	/**
	 * Populate system parameters table with the values read from runtime.
	 * 
	 * @param filter
	 * @throws Exception
	 */
	public void synchronizeCatalog(Param_Ds filter) throws Exception {
		if (!Session.user.get().isSystemUser()) {
			throw new Exception(
					"This operation is allowed only for system users.");
		}
		((IParamService) this.findEntityService(Param.class))
				.doSynchronizeCatalog();
	}

}
