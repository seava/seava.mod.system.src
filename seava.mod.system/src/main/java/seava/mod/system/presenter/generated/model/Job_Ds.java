/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNT_Ds;
import seava.mod.system.domain.generated.model.Job;

@Ds(entity=Job.class, sort={@SortField(field=Job_Ds.f_name)})
public class Job_Ds extends AbstractTypeNT_Ds<Job> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_Job_Ds";
	
	public static final String f_javaClass = "javaClass";
	
	@DsField
	private String javaClass;
	
	public Job_Ds() {
		super();
	}
	
	public Job_Ds(Job e) {
		super(e);
	}
	
	public String getJavaClass() {
	  return this.javaClass;
	}
	
	public void setJavaClass(String javaClass) {
	  this.javaClass = javaClass;
	}
}
