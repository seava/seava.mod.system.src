/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.Job;

@Ds(entity=Job.class, sort={@SortField(field=JobLov_Ds.f_name)})
public class JobLov_Ds extends AbstractTypeNTLov_Ds<Job> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_JobLov_Ds";
	
	
	public JobLov_Ds() {
		super();
	}
	
	public JobLov_Ds(Job e) {
		super(e);
	}
}
