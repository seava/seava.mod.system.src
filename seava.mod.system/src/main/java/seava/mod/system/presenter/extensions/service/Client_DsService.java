/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.extensions.service;

import java.io.File;
import java.util.List;

import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.base.descriptor.DataPackage;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.mod.system.business_api.generated.spi.IClientService;
import seava.mod.system.domain.generated.model.Client;
import seava.mod.system.presenter.generated.model.Client_Ds;
import seava.mod.system.presenter.generated.model.Client_DsParam;

public class Client_DsService extends
		seava.mod.system.presenter.generated.service.Client_DsService {

	/**
	 * Custom query. If not a system user, return only the current client.
	 */
	@Override
	public List<Client_Ds> find(
			IQueryBuilder<Client_Ds, Client_Ds, Client_DsParam> builder)
			throws ManagedException {
		if (!Session.user.get().isSystemUser()) {
			builder.getFilter().setId(Session.user.get().getClientId());
		}
		return super.find(builder);
	}

	/**
	 * Custom insert
	 */
	@Override
	protected void onInsert(List<Client_Ds> list, List<Client> entities,
			Client_DsParam params) throws ManagedException {

		IClientService srv = (IClientService) this.getEntityService();
		String initFileLocation = params.getInitFileLocation();
		if (initFileLocation != null && !"".matches(initFileLocation)) {

			File f = new File(initFileLocation);
			if (!f.exists()) {
				throw new ManagedException(ErrorCodeCommons.FILE_NOT_FOUND,
						"Cannot find initial data descriptor file ("
								+ params.getInitFileLocation() + "). ");
			}

			if (!f.canRead()) {
				throw new ManagedException(ErrorCodeCommons.FILE_NOT_READABLE,
						"Read acces not enabled for initial data descriptor file ("
								+ params.getInitFileLocation() + ") . ");
			}

			IImportDataPackage dp = DataPackage.forIndexFile(params
					.getInitFileLocation());

			for (Client e : entities) {
				srv.doInsertWithUserAccountAndSetup(e,
						params.getAdminUserCode(), params.getAdminUserName(),
						params.getAdminUserLogin(), params.getAdminPassword(),
						dp);
			}
		} else {
			for (Client e : entities) {
				srv.doInsertWithUserAccount(e, params.getAdminUserCode(),
						params.getAdminUserName(), params.getAdminUserLogin(),
						params.getAdminPassword());
			}
		}

	}

	@Override
	protected boolean canInsert() {
		return this.canChange();
	}

	@Override
	protected boolean canUpdate() {
		return this.canChange();
	}

	@Override
	protected boolean canDelete() {
		return this.canChange();
	}

	private boolean canChange() {
		return Session.user.get().isSystemUser();
	}

}
