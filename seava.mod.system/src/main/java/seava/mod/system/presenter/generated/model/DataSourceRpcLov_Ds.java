/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.DataSourceRpc;

@Ds(entity=DataSourceRpc.class, sort={@SortField(field=DataSourceRpcLov_Ds.f_name)})
public class DataSourceRpcLov_Ds extends AbstractTypeNTLov_Ds<DataSourceRpc> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceRpcLov_Ds";
	
	public static final String f_dataSourceId = "dataSourceId";
	public static final String f_dataSourceName = "dataSourceName";
	
	@DsField(join="left", path="dataSource.id")
	private String dataSourceId;
	
	@DsField(join="left", path="dataSource.name")
	private String dataSourceName;
	
	public DataSourceRpcLov_Ds() {
		super();
	}
	
	public DataSourceRpcLov_Ds(DataSourceRpc e) {
		super(e);
	}
	
	public String getDataSourceId() {
	  return this.dataSourceId;
	}
	
	public void setDataSourceId(String dataSourceId) {
	  this.dataSourceId = dataSourceId;
	}
	
	public String getDataSourceName() {
	  return this.dataSourceName;
	}
	
	public void setDataSourceName(String dataSourceName) {
	  this.dataSourceName = dataSourceName;
	}
}
