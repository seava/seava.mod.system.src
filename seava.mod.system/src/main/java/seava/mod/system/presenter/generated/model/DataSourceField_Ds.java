/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNT_Ds;
import seava.mod.system.domain.generated.model.DataSourceField;

@Ds(entity=DataSourceField.class, sort={@SortField(field=DataSourceField_Ds.f_name)})
public class DataSourceField_Ds extends AbstractTypeNT_Ds<DataSourceField> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceField_Ds";
	
	public static final String f_dataType = "dataType";
	public static final String f_dataSourceId = "dataSourceId";
	public static final String f_dataSource = "dataSource";
	
	@DsField
	private String dataType;
	
	@DsField(join="left", path="dataSource.id")
	private String dataSourceId;
	
	@DsField(join="left", path="dataSource.name")
	private String dataSource;
	
	public DataSourceField_Ds() {
		super();
	}
	
	public DataSourceField_Ds(DataSourceField e) {
		super(e);
	}
	
	public String getDataType() {
	  return this.dataType;
	}
	
	public void setDataType(String dataType) {
	  this.dataType = dataType;
	}
	
	public String getDataSourceId() {
	  return this.dataSourceId;
	}
	
	public void setDataSourceId(String dataSourceId) {
	  this.dataSourceId = dataSourceId;
	}
	
	public String getDataSource() {
	  return this.dataSource;
	}
	
	public void setDataSource(String dataSource) {
	  this.dataSource = dataSource;
	}
}
