/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNT_Ds;
import seava.mod.system.domain.generated.model.JobParam;

@Ds(entity=JobParam.class, sort={@SortField(field=JobParamLov_Ds.f_name)})
public class JobParamLov_Ds extends AbstractTypeNT_Ds<JobParam> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_JobParamLov_Ds";
	
	
	public JobParamLov_Ds() {
		super();
	}
	
	public JobParamLov_Ds(JobParam e) {
		super(e);
	}
}
