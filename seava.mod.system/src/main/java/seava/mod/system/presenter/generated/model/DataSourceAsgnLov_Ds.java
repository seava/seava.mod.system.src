/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.DataSource;

@Ds(entity=DataSource.class,jpqlWhere=" e.isAsgn = true ", sort={@SortField(field=DataSourceAsgnLov_Ds.f_name)})
public class DataSourceAsgnLov_Ds extends AbstractTypeNTLov_Ds<DataSource> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceAsgnLov_Ds";
	
	
	public DataSourceAsgnLov_Ds() {
		super();
	}
	
	public DataSourceAsgnLov_Ds(DataSource e) {
		super(e);
	}
}
