/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNT_Ds;
import seava.mod.system.domain.generated.model.DataSourceRpc;

@Ds(entity=DataSourceRpc.class, sort={@SortField(field=DataSourceRpc_Ds.f_name)})
public class DataSourceRpc_Ds extends AbstractTypeNT_Ds<DataSourceRpc> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSourceRpc_Ds";
	
	public static final String f_dataSourceId = "dataSourceId";
	public static final String f_dataSource = "dataSource";
	
	@DsField(join="left", path="dataSource.id")
	private String dataSourceId;
	
	@DsField(join="left", path="dataSource.name")
	private String dataSource;
	
	public DataSourceRpc_Ds() {
		super();
	}
	
	public DataSourceRpc_Ds(DataSourceRpc e) {
		super(e);
	}
	
	public String getDataSourceId() {
	  return this.dataSourceId;
	}
	
	public void setDataSourceId(String dataSourceId) {
	  this.dataSourceId = dataSourceId;
	}
	
	public String getDataSource() {
	  return this.dataSource;
	}
	
	public void setDataSource(String dataSource) {
	  this.dataSource = dataSource;
	}
}
