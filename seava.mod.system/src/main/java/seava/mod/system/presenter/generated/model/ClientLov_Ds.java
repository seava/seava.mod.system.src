/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeNTLov_Ds;
import seava.mod.system.domain.generated.model.Client;

@Ds(entity=Client.class, sort={@SortField(field=ClientLov_Ds.f_code)})
public class ClientLov_Ds extends AbstractTypeWithCodeNTLov_Ds<Client> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_ClientLov_Ds";
	
	
	public ClientLov_Ds() {
		super();
	}
	
	public ClientLov_Ds(Client e) {
		super(e);
	}
}
