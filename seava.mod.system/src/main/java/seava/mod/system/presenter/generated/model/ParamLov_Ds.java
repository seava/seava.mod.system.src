/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeNTLov_Ds;
import seava.mod.system.domain.generated.model.Param;

@Ds(entity=Param.class, sort={@SortField(field=ParamLov_Ds.f_code)})
public class ParamLov_Ds extends AbstractTypeWithCodeNTLov_Ds<Param> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_ParamLov_Ds";
	
	
	public ParamLov_Ds() {
		super();
	}
	
	public ParamLov_Ds(Param e) {
		super(e);
	}
}
