/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNTLov_Ds;
import seava.mod.system.domain.generated.model.DateFormat;

@Ds(entity=DateFormat.class, sort={@SortField(field=DateFormatLov_Ds.f_name)})
public class DateFormatLov_Ds extends AbstractTypeNTLov_Ds<DateFormat> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DateFormatLov_Ds";
	
	
	public DateFormatLov_Ds() {
		super();
	}
	
	public DateFormatLov_Ds(DateFormat e) {
		super(e);
	}
}
