/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeNT_Ds;
import seava.mod.system.domain.generated.model.Client;

@Ds(entity=Client.class, sort={@SortField(field=MyClient_Ds.f_code)})
public class MyClient_Ds extends AbstractTypeWithCodeNT_Ds<Client> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_MyClient_Ds";
	
	
	public MyClient_Ds() {
		super();
	}
	
	public MyClient_Ds(Client e) {
		super(e);
	}
}
