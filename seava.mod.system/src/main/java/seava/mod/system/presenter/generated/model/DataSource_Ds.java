/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeNT_Ds;
import seava.mod.system.domain.generated.model.DataSource;

@Ds(entity=DataSource.class, sort={@SortField(field=DataSource_Ds.f_name)})
public class DataSource_Ds extends AbstractTypeNT_Ds<DataSource> implements IModelWithId<String> {
	
	public static final String ALIAS = "sys_DataSource_Ds";
	
	public static final String f_model = "model";
	public static final String f_isAsgn = "isAsgn";
	
	@DsField
	private String model;
	
	@DsField
	private Boolean isAsgn;
	
	public DataSource_Ds() {
		super();
	}
	
	public DataSource_Ds(DataSource e) {
		super(e);
	}
	
	public String getModel() {
	  return this.model;
	}
	
	public void setModel(String model) {
	  this.model = model;
	}
	
	public Boolean getIsAsgn() {
	  return this.isAsgn;
	}
	
	public void setIsAsgn(Boolean isAsgn) {
	  this.isAsgn = isAsgn;
	}
}
