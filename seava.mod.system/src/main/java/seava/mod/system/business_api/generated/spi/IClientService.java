/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_api.generated.spi;

import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.mod.system.domain.generated.model.Client;

/**
 * Interface to expose business functions specific for {@link Client} domain
 * entity.
 */
public interface IClientService extends IEntityService<Client> {
  
  public void doInsertWithUserAccount(Client client, String userCode, String userName, String loginName, String password)
    throws ManagedException;
  
  public void doInsertWithUserAccountAndSetup(Client client, String userCode, String userName, String loginName, String password, IImportDataPackage dataPackage)
    throws ManagedException;
}
