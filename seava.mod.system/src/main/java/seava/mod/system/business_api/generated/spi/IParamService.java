/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.system.business_api.generated.spi;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.mod.system.domain.generated.model.Param;

/**
 * Interface to expose business functions specific for {@link Param} domain
 * entity.
 */
public interface IParamService extends IEntityService<Param> {
  
  public void doSynchronizeCatalog()
    throws ManagedException;
}
